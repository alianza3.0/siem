# Tarea SIEM
## Scripts : 
 - startsiem.ps1
    - Inicializa las variable de acceso con las que se entra a las computadoras y se inicializa la SIEM
 - managerClient.ps1
    - Obtenemos los ataques de Fuerza bruta y los logs de seguridad del equipo
 - managerServer.ps1
    - Obtenemos los resultados de los logs de seguridad y podemos consultar de diversas manera

    > NOTA: El nombre del archivo que procesa el servidor es a22.xml