#=================Script para recuperar datos de XML y consultas================
# $sesion = New-PSSession -ComputerName "SIMPSONSPC"
# copy -Path ".\registro.xml" -Destination "C:\siem\a22.xml" -ToSession $sesion

#================================Funciones======================================
function inicio{
    echo "Opciones: "
    Write-Host "`t 1: Consultar"
    Write-Host "`t 2: Ordenar"
    Write-Host "`t q: Salir"
}

function sub-busca{
    cls
    echo "Busqueda: "
    Write-Host "`t 1: Tiempo del incidente"
    Write-Host "`t 2: SID del perpetuador"
    Write-Host "`t 3: Cuenta del perpetuador"
    Write-Host "`t 4: Dominio del perpetuador"
    Write-Host "`t 5: NETBIOS del perpetuador"
    Write-Host "`t 6: Direccion IP de origen"
    "=============================================="
    Write-Host "`t 7: SID del perpetuado"
    Write-Host "`t 8: Cuenta del perpetuado"
    Write-Host "`t 9: Dominio del perpetuado"
    Write-Host "`t 10: NETBIOS del perpetuado"
    "----------------------------------------------"
    Write-Host "`t q: Salir"
}

#Obtiene solo los valores
function busca($subcat, $patron){
foreach($x in $hashXML.Values){ 
    if($x.$subcat -match $patron){
    	Write-Output $x; "`n" 
     }
    }
}

#Ordenar
function ordena($subcat){
	($hashXML | foreach {$_.Values} | ? {$_.keys -eq $subcat}).$subcat | sort
}

function localUserExists{
	$usuarios = Get-LocalUser
  foreach($registro in $hashXML.values){
  	foreach($nombre in $usuarios.Name){
    	$nombre
      $registro.BadUser
    	if ($usuarios.Name -like $registro.BadUser){
					"El usuario" + $registro.BadUser + "existe en el equipo"
    		}
    	else{
				"Usuario " + $registro.BadUser + " no encontrado"
    }
    }
  }
}

function domainUserExists{
  foreach($registro in $hashXML.values){
	  try{
				Get-ADUser -Identity $registro.BadUser > null
				"El usuario" + $registro.BadUser + "existe en el dominio"
    }catch{
				"Usuario " + $registro.BadUser + " no encontrado"
    }
  }
}

function domainMachineExists{
  foreach($registro in $hashXML.values){
	  try{
				Get-ADComputer -Identity $registro.BadMachine > null
				"El usuario" + $registro.BadMachine + "existe en el dominio"
    }catch{
       "Usuario " + $registro.BadUser + " no encontrado"
    }
  }
}

#==========================Main=======================
#Importamos el archivo .xml
$hashXML = Import-Clixml -Path .\a22.xml
#Mostramos los registros obtenidos
$hashXML
#Mostramos el contenido de los mismos
$hashXML.Values
#Corremos el menu de consulta
inicio