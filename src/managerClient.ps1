#=============Script para obtener intentos de fuerza burta======================
# $sesion = New-PSSession -ComputerName "SIMPSONSPC"
# copy -Path ".\registro.xml" -Destination "C:\siem\a22.xml" -ToSession $sesion

#=============================Cliente===========================================
function getLogPolicy(){
	auditpol /get /Category:"Logon/Logoff"
}

function setLogPolicy(){
  auditpol /set /category:"System","Account Management","Account Logon","Logon/Logoff","Policy Change" /failure:enable /success:enable     
  auditpol /set /category:"DS Access","Object Access" /failure:enable
}

#Para obtener todos los logs de sguridad
#Get-EventLog -LogName Security

function getBruteForce{
	#Se crea una tabla hash vacía
  $register = @()
  #Se obtienen todas las logs del sistema hasta ese momento
	$logs = Get-EventLog -LogName Security -ComputerName "."
  #Iteramos cada log generado
	foreach ($log in $logs) {
		if($log.instanceid -eq 4625) {
		$type = "Inicio fallido"
    $time = $log.TimeWritten
    <#Usaremos la propiedad ReplacementStrings pues es la que contiene la mayor 
    parte de la información de un log de acceso. 
    Dado a que es un conjunto de cadenas de texto, indexamos el lugar que 
    corresponde a los datos que queremos volcar.
    Las únicas excepciones serían los datos de:
    Time: Que se obtienen de una propiedad existente en el log
    GoodMachine: Que hace referencia al quipo en el cuál se registró el log
    #>
    $who = $log.ReplacementStrings[13]
		$hash = @{Time = $time;
			BadSID = $log.ReplacementStrings[0]; 
			BadUser = $log.ReplacementStrings[1]; 
			BadDomain = $log.ReplacementStrings[2]; 
			BadMachine = $who; 
			BadIP = $log.ReplacementStrings[19]; 
			GoodSID = $log.ReplacementStrings[4]; 
			GoodUser = $log.ReplacementStrings[5]; 
			GoodDomain = $log.ReplacementStrings[6]; 
			GoodMachine = $log.MachineName;}
		$register += @{$type = $hash}
		}
    #
    if( (($log.instanceid -eq 4624) -and ($log.ReplacementStrings[13] -match $register.Values.BadMachine)) -and ($log.TimeWritten -gt $register.Values.Time) ) {
		$type = "Inicio correcto"
		$hash = @{Time = $log.TimeWritten;
			BadSID = $log.ReplacementStrings[0]; 
			BadUser = $log.ReplacementStrings[1]; 
			BadDomain = $log.ReplacementStrings[2]; 
			BadMachine = $log.ReplacementStrings[13]; 
			BadIP = $log.ReplacementStrings[19]; 
			GoodSID = $log.ReplacementStrings[4]; 
			GoodUser = $log.ReplacementStrings[5]; 
			GoodDomain = $log.ReplacementStrings[6]; 
			GoodMachine = $log.MachineName;}
		$register += @{$type = $hash}
		}
	}
#Mostramos los datos obtenidos
#$register
#Obtenemos la ubicación actual
$Path = Get-Location
#Declaramos la variable con el nombre del archivo.xml
$filename = "a22.xml"
#Pasamos la tabla hash que creamos a un archivo xml
$register | Export-Clixml -Path ".\a22.xml"
#Indicamos creación del archivo
#"Se creo el archivo " + $filename + " en "  $Path
}

#--------------------------ConfigInicial----------------------------------------


#**************Desde un powershell de administrador*****************************
#Mostramos la configuración de la política de auditoria de logs
#getLogPolicy
#Configuramos la política de auditoria de logs
setLogPolicy

#----------------Obtiene intentos de fuerza bruta-------------------------------
getBruteForce