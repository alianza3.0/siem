<#******************************************************************************
  ==============================Tarea/Proyecto===============================
  Event Viewer
  #Forward Events - Manda los eventos de un equipo a otro

  Hacer en un solo script los siguientes módulos
  0) Dictamine si este es un host o un server
  	Envia banderas
		-server
	  -client <machineName> <credentials> 
  1) Script que habilite un servidor para recibir eventos
  	
  2) Hacer script que envie eventos a ese servidor (con parámetros y cositas gggg)
  	[Por menu]
		3) SIEM que detecte ataque de fuerza bruta (que eventos buscar y poner un periodo de incidencia)
  		5) Consultas: Qué usuario está iniciando sesión en un equipo, y qué usuarios existen en un equipo
		[Dar equipo (del dominio), da usuarios (del dominio). Da usuarios, da equipo en el que vive(?)]
		(Para todos los registros o en un periodo definido)

  4) Hacer ataque de fuerza bruta a un equipo o red
  
  Aplicar lo anterior en un dominio
  El script solo siempre se ejecuta en el server (si se busca un equipo, va a ese a ejecutarlo)
  Si es cliente, hace config remota, si es el server, hace la config local
  [Debe checar que la config esté realizada (crear un registro de sistema único tal vez?)]
  Hacer con una cuenta de Domain Admin
#>
#############################################################################################
#+++ Configuracion ********************************************************

#Nombre de la máquina virtual del server
$scriptToClient = ".\src\managerClient.ps1"
$scriptToServer = ".\src\managerServer.ps1"

$VMClient = "win10simpsons" # VM Name Hyper-V
$VMServer = "simpsonsG" # VM Name Hyper-V
$usernameOfClientVM = "SIMPSONS\Administrator" # Nombre del usuario de la máquina cliente VM
$usernameOfServerVM = "SIMPSONS\Administrator" # Nombre del usuario de la máquina server VM
$passOfClientVM = "hola123.,"
$passOfServerVM = "hola123.,"

# Accesos para las cuentas virtuales
$nameOfClient = "win10simpsons" # Computer Name (W10SIMPSONS)
$nameOfServer = "PRUEBA" # Computer Name (SIMPSONSPC)
$usernameOfClient = "SIMPSONS\Administrator" # Nombre del usuario de la máquina por ComputerName
$usernameOfServer = "SIMPSONS\Administrator" # Nombre del usuario de la máquina por ComputerName
$passOfClient = "hola123.,"
$passOfServer = "hola123.,"

# ********* Automatizado *********
# ********* Automatizado *********
# ********* Automatizado *********

# Creacion de password para el cliente Virtual Machine
$realPassOfClientVM = ConvertTo-SecureString -String $passOfClientVM -AsPlainText -Force
$realPassOfServerVM = ConvertTo-SecureString -String $passOfServerVM -AsPlainText -Force

# Creacion de password para el server
$realPassOfClient = ConvertTo-SecureString -String $passOfClient -AsPlainText -Force
$realPassOfServer = ConvertTo-SecureString -String $passOfServer -AsPlainText -Force

# Creacion de credenciales por Virtual Machine Hyper-V
$credentialForClientVM = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $usernameOfClientVM, $realPassOfClientVM
$credentialForServerVM = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $usernameOfServerVM, $realPassOfServerVM

# Creacion de credenciales por ComputerName
$credentialForClient = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $usernameOfClient, $realPassOfClient
$credentialForServer = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $usernameOfServer, $realPassOfServer

# Creacion de sesiones por ComputerName
$sesionClient = New-PSSession -ComputerName $nameOfClient -Credential $CredentialCore
$sesionServer = New-PSSession -VMName $VMServer -Credential $credentialForServerVM

#+++ Fin de la configuracion ********************************************************
#=================================================================================

# Entrada a los dominios por Computer Name
# Enter-PSSession -ComputerName $nameOfClient -Credential $CredentialCore
# Enter-PSSession -ComputerName $nameOfServer -Credential $CredentialCore
# Invoke-Command -FilePath .\src\manager.ps1 -VMName $VMServer -Credential $credentialForServerVM

# Entrada a los dominios por Virtual Machine of Hyper-V
# Enter-PSSession -VMName $VMClient -Credential $credentialForClientVM
# Enter-PSSession -VMName $VMServer -Credential $credentialForServerVM
# Invoke-Command -FilePath .\src\manager.ps1 -VMName $VMServer -Credential $credentialForServerVM

#************************************************************************************
#************************************************************************************
#************************************************************************************


#Funcion inicializadora de SIEM
function initSIEM {
	Param([Switch]$server, [Switch]$client)

	#Funcion que configura al servidor y al cliente
	function configServerAndClient {
		"Configurando el server y el cliente..."
		Invoke-Command -FilePath $scriptToServer -VMName $VMServer -Credential $credentialForServerVM
		Invoke-Command -FilePath $scriptToClient -VMName $VMClient -Credential $credentialForClientVM
	}

	#Funcion que configura al servidor
	function configServer {
		"Configurando el server..."
		Invoke-Command -FilePath $scriptToServer -VMName $VMServer -Credential $credentialForServerVM
	}

	#Funcion que configura al cliente
	function configCliente {
		"Configurando el cliente..."
		#Copiamos el script de cliente al cliente
		copy -Path $scriptToClient -Destination "C:\managerClient.ps1" -ToSession $sesionClient
		#Lo corremos
		Invoke-Command -FilePath $scriptToClient -VMName $VMClient -Credential $credentialForClientVM
		#Recuperamos el xml
		copy -Path "C:\a22.xml" -Destination ".\" -FromSession $sesionClient
	}

	#Funcion que retorna la sintaxis correcta
	function invalidArgs {
		"Argumentos invalidos: Debe usarse la siguiente sintaxis `initSIEM 'machineName' <credentials> [-server] [-client] "
	}

	# Comprueba que funcion debe ejecutar dados los [Switch]
	if ($server -and $client) {
		configServerAndClient
	} elseif($server) {
		configServer
	} elseif($client) {
		configCliente
	} else {
		invalidArgs
	}
}

# Funcion Menu
function menuSIEM {
	function doMenu{
		"Selecciona tu opcion: "
		"`t1: Configurar Server"
		"`t2: Configurar Cliente"
		"`t3: Configurar Server y cliente"
		"`tq: Salir"
		selectOption
	}

	function selectOption {
		$choice = Read-Host "Selecciona el numero de la opcion"
		switch ($choice) {
			'1' {
				initSIEM -server
			} 
			'2' {
				initSIEM -client
			} 
			'3' {
				initSIEM -server -client
			} 
			'q' {
				"Adios!"
				return
			}
			default {
				cls
				'Opcion invalida'
				$choice = Read-Host "Presiona Enter para continuar"
				doMenu
			}
		}
	}
	doMenu
}

#==================================Main========================================



menuSIEM